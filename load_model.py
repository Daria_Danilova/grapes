from tensorflow.keras.models import load_model
import tensorflow

model = load_model('health_alexNet_model.h5')

img_height, img_width = 227, 227
test_ds = tensorflow.keras.preprocessing.image_dataset_from_directory(
  'D:/health_test_dataset',
  seed=42,
  image_size=(img_height, img_width),
  batch_size=4)

scores = model.evaluate_generator(test_ds, 142//4)[1]*100
print(scores)

