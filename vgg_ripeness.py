from keras.layers import Input,Lambda,Dense,Flatten,Conv2D
from keras.models import Model 
from keras.applications.vgg16 import VGG16 
from keras.applications.vgg16 import preprocess_input
from keras.preprocessing import image
from keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.models import Sequential
from tensorflow.keras.losses import sparse_categorical_crossentropy
from tensorflow.keras.optimizers import Adam
from kerastuner.tuners import RandomSearch
import tensorflow
from tensorflow.keras import layers
import numpy as np

# Model configuration
batch_size = 32
img_width, img_height = 50, 50

image_count = 1593*2
print(image_count)



dirictory = 'D:/ripeness_dataset/for binary'

train_ds = tensorflow.keras.preprocessing.image_dataset_from_directory(
  dirictory,
  validation_split=0.2,
  subset="training",
  seed=123,
  image_size=(img_height, img_width),
  batch_size=batch_size)
  
val_ds = tensorflow.keras.preprocessing.image_dataset_from_directory(
  dirictory,
  validation_split=0.2,
  subset="validation",
  seed=123,
  image_size=(img_height, img_width),
  batch_size=batch_size)  

normalization_layer = tensorflow.keras.layers.experimental.preprocessing.Rescaling(1./255)


normalized_ds = train_ds.map(lambda x, y: (normalization_layer(x), y))
image_batch, labels_batch = next(iter(normalized_ds))
first_image = image_batch[0]
# Notice the pixels values are now in `[0,1]`.
print(np.min(first_image), np.max(first_image))


AUTOTUNE = tensorflow.data.AUTOTUNE

train_ds = train_ds.cache().prefetch(buffer_size=AUTOTUNE)
val_ds = val_ds.cache().prefetch(buffer_size=AUTOTUNE)


# Определяем размер изображения
IMAGE_SIZE = [224, 224]
# загружаем модель
vgg = VGG16(input_shape=[img_width, img_height] + [3], weights='imagenet', include_top=False)

# замораживаем слои
for layer in vgg.layers:  
    layer.trainable = False
    
# добавляем последний слой с 2 узлами для прогнозирования
x = Flatten()(vgg.output)
prediction = Dense(2, activation='softmax')(x)
model = Model(inputs=vgg.input, outputs=prediction)

# Генерируем Summary
model.summary()

model.compile(
  optimizer='adam',
  loss=tensorflow.losses.SparseCategoricalCrossentropy(from_logits=True),
  metrics=['accuracy'])


history = model.fit(
  train_ds,
  validation_data=val_ds,
  epochs=5
)


import matplotlib.pyplot as plt

acc = history.history['accuracy']
val_acc = history.history['val_accuracy']

loss = history.history['loss']
val_loss = history.history['val_loss']

epochs_range = range(5)

plt.figure(figsize=(12,4))
plt.subplot(1, 2, 1)
plt.plot(epochs_range, acc, label='Точность на обучении')
plt.plot(epochs_range, val_acc, label='Точность на валидации')
plt.legend(loc='lower right')
plt.title('Точность на обучающих и валидационных данных')

plt.subplot(1, 2, 2)
plt.plot(epochs_range, loss, label='Потери на обучении')
plt.plot(epochs_range, val_loss, label='Потери на валидации')
plt.legend(loc='upper right')
plt.title('Потери на обучающих и валидационных данных')
plt.savefig('./foo1.png')
plt.show()


model.save("vgg_ripeness.h5")




