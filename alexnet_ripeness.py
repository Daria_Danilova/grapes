from keras.layers import Input,Lambda,Dense,Flatten,Conv2D
from keras.models import Model 
from keras.applications.vgg16 import VGG16 
from keras.applications.vgg16 import preprocess_input
from keras.preprocessing import image
from keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.models import Sequential
from tensorflow.keras.losses import sparse_categorical_crossentropy
from tensorflow.keras.optimizers import Adam
from kerastuner.tuners import RandomSearch
import tensorflow
from tensorflow.keras.optimizers import SGD
import keras
from tensorflow.keras import layers
import numpy as np

model = keras.models.Sequential([
    keras.layers.Conv2D(filters=96, kernel_size=(11,11), strides=(4,4), activation='relu', input_shape=(227,227,3)),
    keras.layers.BatchNormalization(),
    keras.layers.MaxPool2D(pool_size=(3,3), strides=(2,2)),
    keras.layers.Conv2D(filters=256, kernel_size=(5,5), strides=(1,1), activation='relu', padding="same"),
    keras.layers.BatchNormalization(),
    keras.layers.MaxPool2D(pool_size=(3,3), strides=(2,2)),
    keras.layers.Conv2D(filters=384, kernel_size=(3,3), strides=(1,1), activation='relu', padding="same"),
    keras.layers.BatchNormalization(),
    keras.layers.Conv2D(filters=384, kernel_size=(3,3), strides=(1,1), activation='relu', padding="same"),
    keras.layers.BatchNormalization(),
    keras.layers.Conv2D(filters=256, kernel_size=(3,3), strides=(1,1), activation='relu', padding="same"),
    keras.layers.BatchNormalization(),
    keras.layers.MaxPool2D(pool_size=(3,3), strides=(2,2)),
    keras.layers.Flatten(),
    keras.layers.Dense(4096, activation='relu'),
    keras.layers.Dropout(0.5),
    keras.layers.Dense(4096, activation='relu'),
    keras.layers.Dropout(0.5),
    keras.layers.Dense(2, activation='softmax')
])

model.compile(loss='sparse_categorical_crossentropy', 
              optimizer='SGD', metrics=['accuracy'])


model.summary()

import tensorflow

#разбиваем датасет на тренировочный и валидационный: 80% изображений для обучения и 20% для проверки
def split_on_train_val_ds(directory, img_height, img_width, batch_size):
    #image_size - размер изображений после чтения с диска,т.к.пакеты изображений должны иметь одинаковый размер;
    #seed - случайное начальное число для перетасовки.
    train_ds = tensorflow.keras.preprocessing.image_dataset_from_directory(directory,validation_split=0.2,
                                                                           subset="training",seed=123,
                                                                           image_size=(img_height, img_width),
                                                                           batch_size=batch_size)

    val_ds = tensorflow.keras.preprocessing.image_dataset_from_directory(directory,validation_split=0.2,
                                                                         subset="validation",seed=123,
                                                                         image_size=(img_height, img_width),
                                                                         batch_size=batch_size)

    #для повышения производительности используем буферизованную предварительную выборку,
    #чтобы передавать данные с диска без блокировки ввода-вывода
    AUTOTUNE = tensorflow.data.AUTOTUNE
    
    #cache() сохраняет изображения в памяти после их загрузки с диска в течение первой эпохи
    #prefetch() перекрывает предварительную обработку данных и выполнение модели во время обучения
    train_ds = train_ds.cache().prefetch(buffer_size=AUTOTUNE)
    val_ds = val_ds.cache().prefetch(buffer_size=AUTOTUNE)
    
    return train_ds, val_ds

batch_size = 32
img_width, img_height = 227, 227
dirictory = 'D:/ripeness_dataset/for binary'

train_ds,val_ds= split_on_train_val_ds(dirictory, img_width, img_height, batch_size)

history = model.fit(train_ds,
          validation_data=val_ds,
          epochs=10)

import matplotlib.pyplot as plt

acc = history.history['accuracy']
val_acc = history.history['val_accuracy']

loss = history.history['loss']
val_loss = history.history['val_loss']

epochs_range = range(10)

plt.figure(figsize=(14,6))
plt.subplot(1, 2, 1)
plt.plot(epochs_range, acc, label='Точность на обучении')
plt.plot(epochs_range, val_acc, label='Точность на валидации')
plt.legend(loc='lower right')
plt.title('Точность на обучающих и валидационных данных')

plt.subplot(1, 2, 2)
plt.plot(epochs_range, loss, label='Потери на обучении')
plt.plot(epochs_range, val_loss, label='Потери на валидации')
plt.legend(loc='upper right')
plt.title('Потери на обучающих и валидационных данных')
plt.savefig('./foo1.png')
plt.show()

model.save("alexnet_ripeness.h5")